# New team member

Welcome to the Scalability team!

The information below is meant to serve as a quick overview of both the company
and the team resources.

Feel free to skip parts you are already familiar with, but keep in mind that some of the resources might have changed since you read them so a quick refresher could be useful.

In the [Team Member Onboarding Tasks](#team-member-onboarding-tasks) section of this issue, you can find a checklist so you can stay on top of the tasks.

## Company

First, you are likely to want to know where your place is in the company structure. For this you would want to check out [team chart](https://about.gitlab.com/company/team/org-chart/).

[Handbook](https://about.gitlab.com/handbook/) is a number one resource for all things GitLab.
Understanding the [Values](https://about.gitlab.com/handbook/values/) is really important as that sets the tone for all interactions that you will have in the company. If anything is unclear about it, feel free to ask a question in your next 1-1 with your Engineering Manager.

Sooner or later you will have some money related question, so be sure to checkout
[spending company money page](https://about.gitlab.com/handbook/spending-company-money/). Finally, [communication](https://about.gitlab.com/handbook/communication/) will help you understand more of why you are seeing some of the behaviours you might not have encountered in your prior companies.

Of course, it would be great to go through the whole handbook but that is going to be really difficult given the size of it so don't feel bad if you can't read through
all of it. Remember that you can always search!
For now though, you should get to know your team resources a bit better.

## Team

Each team has it's own handbook section, and Scalability is no exception.
Read through the whole [Scalability handbook](https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/) and all the linked resources please!

Consider this your first team task, understanding the Mission, Vision and how work is executed within the team. If you find any typo's or items that could be made clearer, please consider [editing that page](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/source/handbook/engineering/infrastructure/team/scalability/index.html.md.erb) by submitting a Merge Request.

## Staying informed

There is a lot of information flowing around, but to stay on top of most important things you should be a part of `#company-fyi` and `#company-fyi-private` channels in Slack. Important announcements will be made in the aforementioned slack channel, and #team-member-updates will keep you up to date with bonuses, promotions, new hires (see also #new_team_members), departures etc.

In the Engineering organization, you should regularly check [the Engineering Week in Review doc](https://docs.google.com/document/d/1EkfzI85aqw8chYDBf2GLRvjKEa3s0FWHMI3u0DIr-xg/edit?usp=sharing), and if you joined the Slack channels as recommended above you will be reminded weekly; https://about.gitlab.com/handbook/engineering/#keeping-yourself-informed is a good list of other channels you may wish to be part of to keep abreast of general engineering matters.

As for the team events, you are not obligated to be part of any of the meetings
that you are invited to. However, please note your absence by responding to invites in time. Not attending the meeting is no excuse for not reading the agenda items or going through the notes of the meeting.

## Team Member Onboarding Tasks

- [ ] Log into [ops.gitlab.net](https://ops.gitlab.net/users/sign_in) using the "Sign in with Google" button.
    - [ ] Set your username the same as for GitLab.com.
- [ ] Read [the Scalability Team handbook section](https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/).
- [ ] Review [the Infrastructure Department handbook section](https://about.gitlab.com/handbook/engineering/infrastructure/).
    - [Production architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/).
    - [Scaling GitLab on GitLab.com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/148)
- [ ] Ensure you are at the [Team page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml) as a Scalability team member, submitting a Merge Request otherwise.
- [ ] Ensure that you are a member of the following Slack channels:
    - `#g_scalability`: This is where team discussions and other groups make contact with the Scalability team.
    - `#eng-week-in-review`: This is mandatory for the engineering organization.
    - `#infrastructure-lounge`: This is a channel for Reliability teams.
    - `#production`: Channel for GitLab.com related discussions.
    - `#mech_symp_alerts`: This channel contains alerts of badly performing endpoints on GitLab.com.
    - `#feed_alerts-general`: Channel where SLA breach alerts are shown.
- [ ] Optionally, consider joining some social channels
    - `#loc_*`: There are location based channels for employees all over the world. Look through the channels that start with `loc_` to find the one for your area.
    - `#random`: As the name states, random conversations.
    - `#team-member-updates`: Updates about new team members joining, departing and celebrating work anniversaries.
- [ ] Read the [keeping yourself informed](https://about.gitlab.com/handbook/engineering/#keeping-yourself-informed) documentation.
- [ ] Familiarize yourself with the [training resources](#training-resources) listed further down
- [ ] Add the Scalability shared team calendar to your PTO Roots settings: `gitlab.com_3puidsh74uhqdv9rkp3fj56af4@group.calendar.google.com `


## Manager Onboarding Tasks

- [ ] Create a 1-1 document and setup a recurring weekly meeting
- [ ] Invite to the team demo calls and any other team meetings needed
- [ ] Add to GitLab.com [scalability group](https://gitlab.com/gitlab-org/scalability) with the Maintainer level access
- [ ] Add to ops.gitlab.net [scalability group](https://ops.gitlab.net/gitlab-com/scalability) with the Owner level access
- [ ] Add the right team label to this issue

For access requests, you can submit the access requests by creating a [new issue](https://gitlab.com/gitlab-com/access-requests/issues/new) in gitlab-com/access-requests. Use the New Access Request template for the issue, but this template can be a bit daunting and you can create a comment on this issue to receive some assistance.

## Training resources

The list below contains resources that should make it easier to get familiarized with the systems we us as well as some general useful items worth reading.

Must watch items:

1. [ ] [Saturation detail dashboards in Grafana (5m)](https://youtu.be/_0WOoDzHI-4)
1. [ ] [Walk through of the GitLab SLO framework (34m)](https://youtu.be/QULzN7QrAjY)

GitLab Essentials:

1. [ ] [GitLab Architecture](https://docs.gitlab.com/ee/development/architecture.html)
1. [ ] [GitLab Pipeline](https://docs.gitlab.com/ee/ci/pipelines/): This powers much of GitLab's CI/CD.
1. [ ] [Releases](https://about.gitlab.com/handbook/engineering/releases/) and [Deployment](https://gitlab.com/gitlab-org/release/docs/-/tree/master/general/deploy)
1. [ ] [Cloud-native GitLab images](https://gitlab.com/gitlab-org/build/CNG)

Observability

1. [ ] [Monitoring](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/README.md)
1. [ ] [Logging](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/logging)
1. [ ] [Dashboards](https://gitlab.com/gitlab-com/runbooks/-/tree/master/dashboards)
1. [ ] [Capacity Planning](https://about.gitlab.com/handbook/engineering/infrastructure/capacity-planning/)
1. [ ] [Tamland: forecasting for GitLab.com](https://gitlab.com/gitlab-com/gl-infra/tamland)
1. [ ] [Metrics Catalog](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/metrics-catalog)

Practical items:

1. [ ] [Jsonnet tutorial](https://jsonnet.org/learning/tutorial.html)

Nice reading:

1. [ ] [Wisdom of the ages: How Complex Systems Fail](https://www.adaptivecapacitylabs.com/HowComplexSystemsFail.pdf)
1. [ ] [Monitoring Distributed Systems](https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/)
1. [ ] [Service Level Objectives](https://landing.google.com/sre/sre-book/chapters/service-level-objectives/)
1. [ ] [Practical Alerting](https://landing.google.com/sre/sre-book/chapters/practical-alerting/)
1. [ ] [My Philosophy on Alerting](https://docs.google.com/document/d/199PqyG3UsyXlwieHaqbGiWVa8eMWi8zzAn0YfcApr8Q/edit)
1. [ ] [PromQL for mere mortals (video, 20m)](https://youtu.be/4vZ1PqZ_Foc?t=6923), [slides](https://promcon.io/2019-munich/slides/promql-for-mere-mortals.pdf)


/label ~"onboarding"
/label ~"group::Scalability"
