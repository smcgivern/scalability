# frozen_string_literal: true

require "open3"

RSpec.describe "scripts/runner-test" do
  let(:result) { Open3.capture3(env, "scripts/runner-test") }
  let(:stdout) { result[0] }
  let(:stderr) { result[1] }
  let(:status) { result[2].to_i }

  context "when the script succeeds" do
    let(:env) { {"MESSAGE" => "Test message", "SUCCESS" => "true"} }

    it "writes the output to stdout" do
      expect(stdout).to eq("Test message\n")
      expect(stderr).to be_empty
    end

    it "exits with a zero code" do
      expect(status).to be_zero
    end
  end

  context "when the script raises" do
    let(:env) { {"MESSAGE" => "Test failure message", "SUCCESS" => "false"} }

    it "writes the error message to stderr" do
      expect(stdout).to be_empty
      expect(stderr).to eq("Test failure message\n")
    end

    it "exits with a non-zero code" do
      expect(status).not_to be_zero
    end
  end
end
