# frozen_string_literal: true

require "./lib/feature_categories/orphaned_queues"

RSpec.describe FeatureCategories::OrphanedQueues do
  let(:generated_categories) { %w[foo bar baz] }
  let(:repo_categories) { %w[foo bar baz] }
  let(:all_queues) { [{name: "queue_a", feature_category: "foo"}] }
  let(:ee_all_queues) { [{name: "queue_b", feature_category: "bar"}] }

  before do
    {
      "config/feature_categories.yml" => generated_categories,
      "tmp/feature_categories.yml" => repo_categories,
      "tmp/all_queues.yml" => all_queues,
      "tmp/ee_all_queues.yml" => ee_all_queues
    }.each do |path, contents|
      allow(YAML).to receive(:load_file).with(path).and_return(contents)
    end
  end

  context "when there are no orphaned queues" do
    it "returns a success message" do
      expect(subject.execute).to include("no orphaned queues")
    end
  end

  context "when there are outdated categories" do
    let(:generated_categories) { %w[foo bar] }

    it "returns a success message" do
      expect(subject.execute).to include("no orphaned queues")
    end

    context "with queues using those categories" do
      let(:ee_all_queues) { [{name: "queue_b", feature_category: "baz"}] }

      it "raises an exception" do
        expect { subject.execute }.to raise_error do |error|
          expect(error.message).to include('Outdated categories: ["baz"]')
          expect(error.message).to include('Orphaned queues: ["queue_b"]')
        end
      end
    end
  end
end
